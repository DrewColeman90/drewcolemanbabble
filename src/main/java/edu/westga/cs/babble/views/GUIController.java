package edu.westga.cs.babble.views;

import java.util.List;

import edu.westga.cs.babble.controllers.WordDictionary;
import edu.westga.cs.babble.model.EmptyTileBagException;
import edu.westga.cs.babble.model.PlayedWord;
import edu.westga.cs.babble.model.Tile;
import edu.westga.cs.babble.model.TileBag;
import edu.westga.cs.babble.model.TileGroup;
import edu.westga.cs.babble.model.TileRack;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;

import javafx.util.converter.NumberStringConverter;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;

/**
 * This class will control all the buttons and other interactive aspects of the
 * BabbleGUI class
 * 
 * @author Drew Coleman
 * @version 08/20/2018
 *
 */
public class GUIController {
	private TileBag bagOfTiles;
	@FXML
	private ListView randomLetters;
	@FXML
	private ListView selectedLetters;
	@FXML
	private TextField scoreField;
	@FXML
	private IntegerProperty theScore;

	public GUIController() {
	}
	
	/**
	 * This method initializes all the FXML variables
	 */
	@FXML
	public void initialize() {
		this.theScore = new SimpleIntegerProperty(0);
		this.scoreField.textProperty().bindBidirectional(this.theScore, new NumberStringConverter());;
		this.newGame();
	}

	/**
	 * New Game button - deletes all words
	 */
	@FXML
	private void newGame() {
		this.theScore.set(0);
		this.resetWord();
		while (this.randomLetters.getItems().size() > 0) {
			this.randomLetters.getItems().remove(0);
		}
		this.setUpTileRack();

	}

	/**
	 * Play Button should throw alerts when empty or miss spelled words are played
	 * If a correct word is played then it should add points to the this.score and
	 * remove the tiles from the word listview
	 */
	@FXML
	private void goPlay() {
		PlayedWord submittedWord = new PlayedWord();
		WordDictionary spellCheck = new WordDictionary();
		String word = "";
		for (int count = 0; count < this.selectedLetters.getItems().size(); count++) {
			Tile tile = new Tile((char) this.selectedLetters.getItems().get(count));
			submittedWord.append(tile);
			word += tile.getLetter();
		}
		if (word == null) {
			Alert notAWord = new Alert(AlertType.INFORMATION);
			notAWord.setTitle("No tiles where selected");
			notAWord.setHeaderText("You must select tiles to build a word");
			notAWord.setContentText("You cannot submit an empty word");
			notAWord.showAndWait();
			ButtonType exit = new ButtonType("exit", ButtonData.CANCEL_CLOSE);
			notAWord.getButtonTypes().setAll(exit);
		}
		if (spellCheck.isValidWord(word)) {
			submittedWord.getScore();
			this.theScore.set(submittedWord.getScore() + this.theScore.get());
			this.selectedLetters.getItems().clear();
			this.setUpTileRack();
		} else {
			Alert notAWord = new Alert(AlertType.INFORMATION);
			notAWord.setTitle("Miss Spelled");
			notAWord.setHeaderText("This is not an acceptable word");
			notAWord.setContentText("Please create an acceptable word");
			notAWord.showAndWait();
			ButtonType exit = new ButtonType("exit", ButtonData.CANCEL_CLOSE);
			notAWord.getButtonTypes().setAll(exit);
		}
	}

	/**
	 * This method sets up the Tile list view with random tiles
	 */
	@FXML
	private void setUpTileRack() {
		this.bagOfTiles = new TileBag();
		int count = this.randomLetters.getItems().size();
		while (count < 7) {
			this.randomLetters.getItems().add(this.drawTileHelper().getLetter());
			count++;
		}
	}

	/**
	 * this is a helper method which sets up the random tray
	 * 
	 * @return
	 */
	private Tile drawTileHelper() {
		Tile tile = null;
		try {
			tile = this.bagOfTiles.drawTile();

		} catch (EmptyTileBagException e) {
			Alert notAWord = new Alert(AlertType.INFORMATION);
			notAWord.setTitle("Out of Tiles");
			notAWord.setHeaderText("You have run out of tiles");
			notAWord.setContentText("You can either finish the tiles you have or start a new game");
			notAWord.showAndWait();
			ButtonType exit = new ButtonType("exit", ButtonData.CANCEL_CLOSE);
			notAWord.getButtonTypes().setAll(exit);
		}
		return tile;
	}

	/**
	 * This method takes selected tiles in the random tray, removes them and adds
	 * them to the selected word tray
	 */
	@FXML
	private void selectWord() {
		try {
			this.selectWordHelper();
		} catch (IllegalArgumentException iae) {
			Alert notAWord = new Alert(AlertType.INFORMATION);
			notAWord.setTitle("Must select a tile");
			notAWord.setHeaderText("Be careful where you select must select a letter");
			notAWord.setContentText("You selected outside a tile - please select again");
			notAWord.showAndWait();
			ButtonType exit = new ButtonType("exit", ButtonData.CANCEL_CLOSE);
			notAWord.getButtonTypes().setAll(exit);
		}
	}

	private void selectWordHelper() {
		if (this.randomLetters.getItems().size() > 0) {
			char letterSelected = 0;
			ObservableList selectedTile = this.randomLetters.getSelectionModel().getSelectedItems();
			Tile addedTile = new Tile(selectedTile.toString().charAt(1));
			if (selectedTile != null) {
				this.randomLetters.getItems().remove(this.randomLetters.getSelectionModel().getSelectedIndex());
				this.selectedLetters.getItems().add(addedTile.getLetter());
			}
		} else {
			Alert notAWord = new Alert(AlertType.INFORMATION);
			notAWord.setTitle("No Tiles");
			notAWord.setHeaderText("You either must a new game to set up your tile rack or select play word");
			notAWord.setContentText("Select new game to begin");
			notAWord.showAndWait();
			ButtonType exit = new ButtonType("exit", ButtonData.CANCEL_CLOSE);
			notAWord.getButtonTypes().setAll(exit);
		}
	}

	/**
	 * This method resets the selected word placing all tiles back into the random
	 * tile tray
	 */
	@FXML
	private void resetWord() {
		int wordSize = this.selectedLetters.getItems().size() - 1;
		for (int count = wordSize; count >= 0; count--) {
			this.randomLetters.getItems().add(this.selectedLetters.getItems().get(count));
			this.selectedLetters.getItems().remove(this.selectedLetters.getItems().get(count));
		}
	}
}
