package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPlayedWordClear {
	PlayedWord testWord = new PlayedWord();
	
	@Test
	void shouldClearEmptyWord() {
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}
	
	@Test
	void shouldClearWordWithOneTile() {
		Tile aA = new Tile('a');
		testWord.append(aA);
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}
	
	@Test
	void shouldClearWordWithManyTiles() {
		Tile gG = new Tile('g');
		Tile aA = new Tile('a');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		testWord.append(gG);
		testWord.append(oO);
		testWord.append(aA);
		testWord.append(tT);
		testWord.clear();
		assertEquals(true, testWord.tiles().isEmpty());
	}

}
