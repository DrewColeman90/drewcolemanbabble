package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPlayedWordGetScore {
	PlayedWord testWord = new PlayedWord();

	@Test
	void emptyWordShouldHaveScoreOfZero() {
		assertEquals(0, testWord.getScore());
	}

	@Test
	void scoreAOneTileWord() {
		Tile aA = new Tile('a');
		testWord.append(aA);
		assertEquals(1, testWord.getScore());
	}

	@Test
	void scoreAWordWithMultipleDifferingTiles() {
		Tile gG = new Tile('g');
		Tile aA = new Tile('a');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		testWord.append(gG);
		testWord.append(oO);
		testWord.append(aA);
		testWord.append(tT);
		assertEquals(5, testWord.getScore());
	}

	@Test
	void scoreAWordContainingDuplicateTiles() {
		Tile gG = new Tile('g');
		Tile ggg = new Tile('g');
		Tile eE = new Tile('e');
		Tile sS = new Tile('s');
		testWord.append(eE);
		testWord.append(gG);
		testWord.append(ggg);
		testWord.append(sS);
		assertEquals(6, testWord.getScore());
	}

}
