package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPlayedWordMatches {
	PlayedWord testWord = new PlayedWord();
	
	@Test
	void hasTilesForAMultipleLetterWord() {
		Tile gG = new Tile('g');
		Tile aA = new Tile('a');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		testWord.append(gG);
		testWord.append(oO);
		testWord.append(aA);
		testWord.append(tT);
		assertEquals(true, testWord.matches("GOAT"));
	}

	@Test
	void hasTilesForASingleLetterWord() {
		Tile aA = new Tile('a');
		testWord.append(aA);
		assertEquals(true, testWord.matches("A"));
	}

	@Test
	void cannotMatchWordWhenTilesAreScrambled() {
		Tile gG = new Tile('g');
		Tile aA = new Tile('a');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		testWord.append(gG);
		testWord.append(aA);
		testWord.append(tT);
		testWord.append(oO);
		assertEquals(false, testWord.matches("GOAT"));
	}

	@Test
	void cannotMatchWordIfInsufficientTiles() {
		Tile gG = new Tile('g');
		Tile aA = new Tile('a');
		Tile oO = new Tile('o');
		testWord.append(gG);
		testWord.append(oO);
		testWord.append(aA);
		assertEquals(false, testWord.matches("GOAT"));
	}

	@Test
	void canMatchWordWithDuplicateLetters() {
		Tile gG = new Tile('g');
		Tile ggg = new Tile('g');
		Tile eE = new Tile('e');
		Tile sS = new Tile('s');
		testWord.append(eE);
		testWord.append(gG);
		testWord.append(ggg);
		testWord.append(sS);
		assertEquals(true, testWord.matches("EGGS"));
	}

	@Test
	void nonEmptyWordShoudlNotMatchEmptyText() {
		Tile gG = new Tile('g');
		Tile ggg = new Tile('g');
		Tile eE = new Tile('e');
		Tile sS = new Tile('s');
		testWord.append(eE);
		testWord.append(gG);
		testWord.append(ggg);
		testWord.append(sS);
		assertEquals(false, testWord.matches(""));
	}

	@Test
	void emptyWordShouldNotMatchEmptyText() {
		assertEquals(false, testWord.matches(""));
	}

	@Test
	void shouldNotAllowNull() {
		try {
			testWord.matches(null);
		} catch (IllegalArgumentException iae) {
			assert (true);
		}
	}

}
