package edu.westga.cs.babble.model;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class TestTileBagDrawTile {

	@Test
	public void canDrawAllTiles() throws EmptyTileBagException {
		TileBag bag = new TileBag();
		for (int count = 0; count < 98; count++) {
			bag.drawTile();
		}
		assertEquals(0, bag.getSize());
	}

	@Test
	public void canNotDrawTooManyTiles() throws EmptyTileBagException {
		try {
			TileBag bag = new TileBag();
			for (int count = 0; count < 99; count++) {
				bag.drawTile();
			}
		} catch (EmptyTileBagException etbe) {
			assertTrue(true);
		} catch (Exception e) {
			fail("wrong exception thrown");
		}
	}

	@Test
	public void hasProperTileDistribution() throws EmptyTileBagException {
		ArrayList<Tile> point1Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point2Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point3Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point4Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point5Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point8Tiles = new ArrayList<Tile>();
		ArrayList<Tile> point10Tiles = new ArrayList<Tile>();
		TileBag bag = new TileBag();
		
		for (int count = 0; count < 98; count++) {
			Tile drawnTile = bag.drawTile();
			if (drawnTile.getPointValue() == 1) {
				point1Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 2) {
				point2Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 3) {
				point3Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 4) {
				point4Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 5) {
				point5Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 8) {
				point8Tiles.add(drawnTile);
			} else if (drawnTile.getPointValue() == 10) {
				point10Tiles.add(drawnTile);
			}
		}
		assertEquals(0, bag.getSize());
		assertEquals(68, point1Tiles.size());
		assertEquals(7, point2Tiles.size());
		assertEquals(8, point3Tiles.size());
		assertEquals(10, point4Tiles.size());
		assertEquals(1, point5Tiles.size());
		assertEquals(2, point8Tiles.size());
		assertEquals(2, point10Tiles.size());

	}
}
