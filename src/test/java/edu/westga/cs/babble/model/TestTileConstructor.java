package edu.westga.cs.babble.model;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TestTileConstructor {

	@Test
	public void shouldNotAllowNonLetters() {
		try {
			Tile badTile8 = new Tile('8');
			badTile8.getLetter();
		} catch (IllegalArgumentException e) {

			assertTrue(true);
		} catch (Exception e) {
			fail("wrong exception thrown");
		}
		
		try {
			Tile badTileHash = new Tile('#');
			badTileHash.getLetter();
		} catch (IllegalArgumentException e) {

			assertTrue(true);
		} catch (Exception e) {
			fail("wrong exception thrown");
		}
		
	}

	@Test
	public void shouldCreateOnePointTiles() {
		Tile eE = new Tile('e');
		Tile rR = new Tile('r');
		Tile lL = new Tile('L');
		Tile uU = new Tile('u');
		Tile aA = new Tile('A');
		assertEquals(1, eE.getPointValue());
		assertEquals(1, rR.getPointValue());
		assertEquals(1, lL.getPointValue());
		assertEquals(1, uU.getPointValue());
		assertEquals(1, aA.getPointValue());
	}

	@Test
	public void shouldCreateTwoPointTiles() {
		Tile dD = new Tile('d');
		Tile gG = new Tile('G');
		assertEquals(2, dD.getPointValue());
		assertEquals(2, gG.getPointValue());
	}

	@Test
	public void shouldCreateThreePointTiles() {
		Tile bB = new Tile('b');
		Tile cC = new Tile('C');
		Tile mM = new Tile('M');
		Tile pP = new Tile('p');
		assertEquals(3, bB.getPointValue());
		assertEquals(3, cC.getPointValue());
		assertEquals(3, mM.getPointValue());
		assertEquals(3, pP.getPointValue());
	}

	@Test
	public void shouldCreateFourPointTiles() {
		Tile fF = new Tile('f');
		Tile hH = new Tile('H');
		Tile vV = new Tile('V');
		Tile wW = new Tile('w');
		Tile yY = new Tile('y');
		assertEquals(4, fF.getPointValue());
		assertEquals(4, hH.getPointValue());
		assertEquals(4, vV.getPointValue());
		assertEquals(4, wW.getPointValue());
		assertEquals(4, yY.getPointValue());
	}

	@Test
	public void shouldCreateFivePointTiles() {
		Tile kK = new Tile('K');
		assertEquals(5, kK.getPointValue());
	}

	@Test
	public void shouldCreateEightPointTiles() {
		Tile xx = new Tile('x');
		Tile jj = new Tile('J');
		assertEquals(8, xx.getPointValue());
		assertEquals(8, jj.getPointValue());
	}

	@Test
	public void shouldCreateTenPointTiles() {
		Tile qQ = new Tile('q');
		Tile zz = new Tile('Z');
		assertEquals(10, qQ.getPointValue());
		assertEquals(10, zz.getPointValue());
	}

}
