package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

class TestTileGroupAppend {
	PlayedWord groupOfTiles = new PlayedWord();

	@Test
	public void shouldNotAllowNull() {
		try {
			groupOfTiles.append(null);
		} catch (IllegalArgumentException iae) {
			assert (true);
		}
	}

	@Test
	public void emptyGroupShouldBeEmpty() {
		assertEquals(true, groupOfTiles.tiles().isEmpty());
	}

	@Test
	public void shouldHaveOneTileInTileGroup() {
		Tile kK = new Tile('k');
		groupOfTiles.append(kK);
		assertEquals(kK, groupOfTiles.tiles().get(0));
		assertEquals("K", groupOfTiles.getHand());
	}

	@Test
	public void shouldHaveManyTilesInTileGroup() {
		Tile aA = new Tile('a');
		Tile gG = new Tile('g');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		groupOfTiles.append(gG);
		groupOfTiles.append(oO);
		groupOfTiles.append(aA);
		groupOfTiles.append(tT);
		assertEquals(gG, groupOfTiles.tiles().get(0));
		assertEquals("GOAT", groupOfTiles.getHand());
	}

	@Test
	public void shouldHaveManyTilesIncludingDuplicatesInTileGroup() {
		Tile aA = new Tile('a');
		Tile gG = new Tile('g');
		Tile oO = new Tile('o');
		Tile tT = new Tile('t');
		Tile bB = new Tile('b');
		Tile sS = new Tile('s');
		groupOfTiles.append(gG);
		groupOfTiles.append(oO);
		groupOfTiles.append(aA);
		groupOfTiles.append(tT);
		groupOfTiles.append(bB);
		groupOfTiles.append(sS);
		assertEquals("GOATBS", groupOfTiles.getHand());
	}

	@Test
	public void canNotAddSameTileTwice() {
		try {
			Tile gG = new Tile('g');
			Tile oO = new Tile('o');
			Tile tT = new Tile('t');
			groupOfTiles.append(gG);
			groupOfTiles.append(oO);
			groupOfTiles.append(oO);
			groupOfTiles.append(tT);
		} catch (IllegalArgumentException iae) {
			assert (true);
		}
	}

}
