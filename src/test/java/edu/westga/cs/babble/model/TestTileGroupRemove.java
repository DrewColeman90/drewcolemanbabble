package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTileGroupRemove {
	TileRack groupOfTiles = new TileRack();
	Tile testConstant = new Tile('c');
	
	@Test
	public void canNotRemoveFromEmptyTileGroup() {
		try {
			groupOfTiles.remove(testConstant);
		} catch (TileNotInGroupException e) {
			assert(true);
		}
	}

	@Test
	public void canNotRemoveTileNotInTileGroup() {
		Tile pP = new Tile('p');
		groupOfTiles.append(pP);
		try {
			groupOfTiles.remove(testConstant);
		} catch (TileNotInGroupException e) {
			assert(true);
		}
		assertEquals(pP, groupOfTiles.tiles().get(0));
	}

	@Test
	public void canRemoveOnlyTileInTileGroup() {
		Tile pP = new Tile('p');
		groupOfTiles.append(pP);
		try {
			groupOfTiles.remove(pP);
		} catch (TileNotInGroupException e) {
			assert(false);
		}
		assertEquals(true, groupOfTiles.tiles().isEmpty());
	}

	@Test
	public void canRemoveFirstOfManyTilesFromTileGroup() {
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		groupOfTiles.append(pP);
		groupOfTiles.append(mM);
		groupOfTiles.append(iI);
		groupOfTiles.append(gG);
		groupOfTiles.append(vV);
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		try {
			groupOfTiles.remove(pP);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("MIGVAB", groupOfTiles.getHand());
	}

	@Test
	public void canRemoveLastOfManyTilesFromTileGroup() {
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		groupOfTiles.append(pP);
		groupOfTiles.append(mM);
		groupOfTiles.append(iI);
		groupOfTiles.append(gG);
		groupOfTiles.append(vV);
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		try {
			groupOfTiles.remove(bB);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("PMIGVA", groupOfTiles.getHand());
	}

	@Test
	public void canRemoveMiddleOfManyTilesFromTileGroup() {
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		groupOfTiles.append(pP);
		groupOfTiles.append(mM);
		groupOfTiles.append(iI);
		groupOfTiles.append(gG);
		groupOfTiles.append(vV);
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		try {
			groupOfTiles.remove(gG);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("PMIVAB", groupOfTiles.getHand());
	}

	@Test
	public void canRemoveMultipleTilesFromTileGroup() {
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		groupOfTiles.append(pP);
		groupOfTiles.append(mM);
		groupOfTiles.append(iI);
		groupOfTiles.append(gG);
		groupOfTiles.append(vV);
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		try {
			groupOfTiles.remove(gG);
			groupOfTiles.remove(mM);
			groupOfTiles.remove(vV);
		} catch (TileNotInGroupException e) {
			e.printStackTrace();
		}
		assertEquals("PIAB", groupOfTiles.getHand());
	}

	@Test
	public void doesNotRemoveDuplicateTilesFromTileGroup() {
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		groupOfTiles.append(pP);
		groupOfTiles.append(mM);
		groupOfTiles.append(iI);
		groupOfTiles.append(gG);
		groupOfTiles.append(vV);
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		try {
			groupOfTiles.remove(gG);
			groupOfTiles.remove(gG);
		} catch (TileNotInGroupException e) {
			assert(true);
		}
		assertEquals("PMIVAB", groupOfTiles.getHand());
	}

}
