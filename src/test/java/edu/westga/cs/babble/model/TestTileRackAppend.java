package edu.westga.cs.babble.model;

class TestTileRackAppend {

	void shouldNotAppendToFullRack() {
		TileRack testRack = new TileRack();
		Tile pP = new Tile('p');
		Tile iI = new Tile('i');
		Tile gG = new Tile('G');
		Tile vV = new Tile('V');
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile mM = new Tile('m');
		Tile oneToMany = new Tile('l');
		try {
			testRack.append(pP);
			testRack.append(mM);
			testRack.append(iI);
			testRack.append(gG);
			testRack.append(vV);
			testRack.append(aA);
			testRack.append(bB);
			testRack.append(oneToMany);
		} catch (TileRackFullException trfe) {
			assert (true);
		}

	}

}
