package edu.westga.cs.babble.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestTileRackGetNumberOfTilesNeeded {
	TileRack groupOfTiles = new TileRack();
	
	@Test
	void emptyTileRackShouldNeedMaxSizeNumberOfTiles() {
		assertEquals(7, groupOfTiles.getNumberOfTilesNeeded());
	}
	
	@Test
	void tileRackWithOneTileShouldNeedMaxSizeMinusOneTiles() {
		Tile aA = new Tile('a');
		groupOfTiles.append(aA);
		assertEquals(6, groupOfTiles.getNumberOfTilesNeeded());
	}
	
	@Test
	void tileRackWithSeveralTilesShouldNeedSomeTiles() {
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile cC = new Tile('c');
		Tile dD = new Tile('d');
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		groupOfTiles.append(cC);
		groupOfTiles.append(dD);
		assertEquals(3, groupOfTiles.getNumberOfTilesNeeded());
	}
	
	@Test
	void fullRackNeedsZeroTiles() {
		Tile aA = new Tile('a');
		Tile bB = new Tile('b');
		Tile cC = new Tile('c');
		Tile dD = new Tile('d');
		Tile eE = new Tile('e');
		Tile fF = new Tile('f');
		Tile gG = new Tile('g');
		groupOfTiles.append(aA);
		groupOfTiles.append(bB);
		groupOfTiles.append(cC);
		groupOfTiles.append(dD);
		groupOfTiles.append(eE);
		groupOfTiles.append(fF);
		groupOfTiles.append(gG);
		assertEquals(0, groupOfTiles.getNumberOfTilesNeeded());
	}

}
